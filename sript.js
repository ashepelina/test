(function () {
    var greating = (function () {
    	var greating = {};
        var hello = 'hello world';

        function sayHello() {
            console.log(hello);
        };

        greating.hey = function() {
            sayHello();
        };

        return greating;
    })();

    if (typeof module !== 'undefined' && typeof module.exports !== 'undefined')
        module.exports = greating;
    else
        window.greating = greating;
}());
